import React, { Component } from 'react';
import '../styles/App.css';

const JSONData = "https://bitbucket.org/GuestOne/goodline-test-task/raw/733ce087f959bd67276db2d6db75d4b49bc004fd/frontend/data.json";

export default class App extends Component{
  constructor(){
    super();
    this.state = {
      error: null,
      isLoaded: false,
      data: []
    }
  }
  componentDidMount(){
    fetch(JSONData)
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            data: result
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }
  render() {
    const { error, isLoaded, data } = this.state;
    if (error) {
      return <div>Loading error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Table is loading...</div>;
    } else {
      return (
        <table>
          <thead>
            <tr>
              <th>Name</th>
              <th>Phone</th>
              <th>Age</th>
            </tr>
          </thead>
          <tbody>
            {data.map(function(item) {
                 return (
                    <tr key = {item.id}>
                        <td>{item.name}</td>
                        <td>{item.phone}</td>
                        <td>{item.age}</td>
                    </tr>
                  )
               })}
          </tbody>
         </table>
      );
    }
  }
}
